---
title: "Porcelain Throne"
date: "2020-05-22"
published: true
---

I recently discovered a Discord group which presents a theme each day, and everyone builds a model based on that theme. The day I joined, the theme was "throne".

Of course the first thing I thought of was a toilet, so here it is 😊

<img src="porcelain_throne.png" width="100%" style="max-width: 512px" />
