---
title: "EIU Clocktower"
date: "2020-04-16"
published: true
---

Not quite as happy with my results this time, but I've definitely learned some things. Things like "don't pick an outdoor scene" and "how to make one tree look like lots of different trees".

Subject here is a clocktower from the university in my hometown. I always liked it and thought it'd be a manageable model to create. Model wasn't bad, but it turns out that outdoor scenes require a lot more framing than just a table and wall like in the donut shop scene 😊.

<video src="/eiu-clocktower.m4v" controls="true" autoplay="true" width="100%" style="max-width: 512px" />
