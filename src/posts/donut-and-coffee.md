---
title: "Donut and Coffee"
date: "2020-04-09"
published: true
---

First of what I plan to be an ongoing series of blender renders. For this one, I was following [this YouTube Series](https://www.youtube.com/watch?v=NyJWoyVx_XI&list=PLjEaoINr3zgEq0u2MzVgAaHEBt--xLB6U) by Blender Guru.

<video src="/donut-and-coffee.mp4" controls="true" autoplay="true" width="100%" style="max-width: 512px" />
