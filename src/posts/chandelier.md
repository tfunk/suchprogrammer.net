---
title: "Chandelier"
date: "2020-06-01"
published: true
---

Chandelier, inspired by the one that's actually hanging in my stairwell. Huzzah!

<img src="chandelier.png" width="100%" style="max-width: 512px" />
