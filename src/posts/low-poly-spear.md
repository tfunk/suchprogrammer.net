---
title: "Spear"
date: "2020-05-26"
published: true
---

A little more game-y than previous posts. This one's just a low-poly spear that's I guess been jammed into a rock. Hopefully someone can get it out.

<img src="spear.png" width="100%" style="max-width: 512px" />
