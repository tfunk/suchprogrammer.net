---
title: "Living Room w/ Fireplace"
date: "2020-05-06"
published: true
---

Indoor scene with multiple lightsources. Few different challenges in this scene, esp surrounding bump/deformation on the fireplace.

I wanted to animation the fireplace, but my computer was struggling with Blender's fire simulation stuff so there's just a static texture there.

Pretty happy with how the wall light fixture turned out. Though I sorta cheated on that by excluding the actual glass from the frame. Works for a render like this but you wouldn't want to get too close 😊


<img src="living_room.png" width="100%" style="max-width: 512px" />
